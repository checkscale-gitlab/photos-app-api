**Photos App (offline)**

So hereby is our test task. It consists of 3 parts:

- task discussion (online, 30 mins)
  - discuss the task
  - highlight missing requirements (intentionally)
- implementation (offline, expected time - 2 evenings, about 6 hours, it’s ok to have something not implemented)
    - use any frameworks/stack you want
    - main focus:
        - api schema (endpoints, data models)
        - project structure
        - working docker-compose.yml with dev environment 
            - bootstraps the whole project with all dependencies 
            - is easy to attach a python debugger from IDE you are using 
            - hot code reload
- review  (online, 30 mins)

**Briefing**

We are working on the “Photos App”. It’s a service, which provides users the ability to upload, store and share their photos. It’s available both as web and as mobile applications, with identical functionality.

The application consist of 4 screens:

- Login / Registration
- Pick photo gallery; gallery list
    - in short - it’s a list with all galleries created by the current user
    - on top of it, there are 2 special options: 
        - all photos 
        - photos not attached to any gallery
    - with options to:
        - rename galleries
        - reorder galleries 
        - delete gallery 
        - get/remove shared link for the gallery
        - create new one
    - View gallery
        - in short, it is a grid with photos.
        - with options to: 
            - reorder photos in the gallery 
            - remove photos from the gallery
            - add photos to the gallery
      - View photo
        - shows detailed information about the selected photo, and also the list of galleries this photo is linked to

**DISCLAIMER**

- Requirements are ambiguous
- Not all points are expected to be implemented

