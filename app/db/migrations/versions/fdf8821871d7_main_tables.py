"""main tables

Revision ID: fdf8821871d7
Revises:
Create Date: 2022-01-27 15:36:44.791880

"""
from typing import Tuple

import sqlalchemy as sa
from alembic import op
from sqlalchemy import func

revision = "fdf8821871d7"
down_revision = None
branch_labels = None
depends_on = None


def create_users_table() -> None:
    op.create_table(
        "users",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("email", sa.Text, unique=True,
                  nullable=False, index=True),
        sa.Column("salt", sa.Text, nullable=False),
        sa.Column("passhash", sa.Text),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            nullable=False,
            server_default=func.now(),
        )
    )


def create_galleries_table() -> None:
    op.create_table(
        "galleries",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.Text, nullable=False, index=True),
        sa.Column("shared_uuid", sa.Text, index=True),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            nullable=False,
            server_default=func.now(),
        )
    )


def create_galleries_sequance() -> None:
    op.execute(
        """
    create sequence galleries_sequance;
    """
    )


def create_galleries_accesses_table() -> None:
    op.create_table(
        "galleries_accesses",
        sa.Column(
            "user_id",
            sa.Integer,
            sa.ForeignKey("users.id", ondelete="CASCADE"),
            nullable=False,
        ),
        sa.Column(
            "gallery_id",
            sa.Integer,
            sa.ForeignKey("galleries.id", ondelete="CASCADE"),
            nullable=False,
        ),
        sa.Column("is_owned", sa.Boolean),
        sa.Column("position", sa.Numeric, nullable=False, unique=True,
                  server_default=func.nextval('galleries_sequance')),
    )


def create_photos_table() -> None:
    op.create_table(
        "photos",
        sa.Column("uuid", sa.Text, primary_key=True),
        sa.Column("metadata", sa.JSON),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            nullable=False,
            server_default=func.now(),
        )
    )


def create_photos_sequance() -> None:
    op.execute(
        """
    create sequence photos_sequance;
    """
    )


def create_photos_attachments_table() -> None:
    op.create_table(
        "photos_attachments",
        sa.Column(
            "gallery_id",
            sa.Integer,
            sa.ForeignKey("galleries.id", ondelete="CASCADE"),
            nullable=False,
        ),
        sa.Column(
            "photo_uuid",
            sa.Text,
            sa.ForeignKey("photos.uuid", ondelete="CASCADE"),
            nullable=False,
        ),
        sa.Column("position", sa.Numeric, nullable=False, unique=True,
                  server_default=func.nextval('photos_sequance')),
    )


def upgrade() -> None:
    create_users_table()
    create_galleries_table()
    create_galleries_sequance()
    create_galleries_accesses_table()
    create_photos_table()
    create_photos_sequance()
    create_photos_attachments_table()


def downgrade() -> None:
    op.drop_table("photos_attachments")
    op.drop_table("photos")
    op.drop_table("galleries_accesses")
    op.drop_table("galleries")
    op.drop_table("users")
