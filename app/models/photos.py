from decimal import Decimal
from typing import Dict, List, Optional
from uuid import UUID

from app.models.mixins import DateTimeModelMixin, PositionedItemMixin
from pydantic import BaseModel


class Photo(BaseModel):
    photo_uuid: UUID


class PositionedPhoto(Photo, PositionedItemMixin):
    pass


class PhotoList(BaseModel):
    photos: List[Optional[Photo]] = []


class PhotoListWithPositions(BaseModel):
    photos: List[Optional[PositionedPhoto]] = []


class PhotoListInResponse(PhotoList):
    file_link_template: str = "../api/photos/\{photo_uuid\}"
    info_link_template: str = "../api/photos/\{photo_uuid\}/info"


class PhotoListInReorderRequest(BaseModel):
    new_index: int
    photos: List[str]


class PhotoInDB(Photo, DateTimeModelMixin):
    metadata: Dict = {}
