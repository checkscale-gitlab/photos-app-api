from decimal import Decimal
from typing import Tuple


# https://begriffs.com/posts/2018-03-20-user-defined-order.html

def find_positions(
    left_element: Decimal,
    right_element: Decimal,
    insertion_index: int,
    new_elements_count: int
) -> Tuple[Decimal, ...]:
    position_step = (right_element - left_element) / (new_elements_count + 1)
    new_position: Decimal = left_element + position_step
    result_positions = []
    for element in range(new_elements_count):
        result_positions.append(new_position)
        new_position += position_step
    return tuple(result_positions)
