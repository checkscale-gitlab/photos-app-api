from uuid import uuid4
from typing import Optional

from app.api.dependencies.auth_user import get_current_user_authorizer
from app.api.dependencies.repository import get_repository
from app.db.errors import EntityDoesNotExist
from app.db.repositories.photos import PhotosRepository
from app.media_storage.errors import MediaDoesNotExist
from app.media_storage.facade import add_photos, del_photos, get_photos
from app.models.galleries import PhotoWithGalleriesInResponse
from app.models.photos import PhotoListInResponse
from app.models.users import UserInDB
from app.resources import strings
from fastapi import (APIRouter, Body, Depends, HTTPException, Response,
                     UploadFile)
from fastapi.responses import FileResponse
from starlette.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

router = APIRouter()


@router.get(
    "",
    status_code=HTTP_200_OK,
    name="Get all photos as list of links",
    response_model=PhotoListInResponse
)
async def get_all_photos(
    user: UserInDB = Depends(get_current_user_authorizer()),
    photos_repo: PhotosRepository = Depends(get_repository(PhotosRepository)),
) -> PhotoListInResponse:
    list_of_photos = await photos_repo.get_photos_by_user_id(user.id)
    return list_of_photos


@router.get(
    "/unattached",
    status_code=HTTP_200_OK,
    name="Get all unattached (to any gallery) photos as list of links",
    response_model=PhotoListInResponse
)
async def get_all_unattached_photos(
    user: UserInDB = Depends(get_current_user_authorizer()),
    photos_repo: PhotosRepository = Depends(get_repository(PhotosRepository)),
) -> PhotoListInResponse:
    list_of_photos = await photos_repo.get_unnatached_photos_by_user_id(user.id)
    return list_of_photos


@router.get(
    "/{photo_uuid}",
    status_code=HTTP_200_OK,
    name="Get photo as media file",
    response_class=FileResponse,
    responses={
        200: {
            "content": {"image": {}}
        }
    },
)
async def get_photo(
    photo_uuid: str,
    user: UserInDB = Depends(get_current_user_authorizer()),
    photos_repo: PhotosRepository = Depends(get_repository(PhotosRepository))
) -> FileResponse:
    try:
        await photos_repo.get_photo_by_photo_uuid(photo_uuid)
        photo_path = await get_photos([photo_uuid])
    except (EntityDoesNotExist, MediaDoesNotExist) as existence_error:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.INCORRECT_PHOTO_UUID,
        ) from existence_error
    return FileResponse(photo_path, media_type="image")


@router.get(
    "/{photo_uuid}/info",
    status_code=HTTP_200_OK,
    name="Get photo info",
    response_model=PhotoWithGalleriesInResponse
)
async def get_photo_info(
    photo_uuid: str,
    user: UserInDB = Depends(get_current_user_authorizer()),
    photos_repo: PhotosRepository = Depends(get_repository(PhotosRepository))
) -> PhotoWithGalleriesInResponse:
    try:
        info = await photos_repo.get_photo_by_photo_uuid(photo_uuid)
    except EntityDoesNotExist as existence_error:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.INCORRECT_PHOTO_UUID,
        ) from existence_error
    return info


@router.post(
    "/upload",
    status_code=HTTP_200_OK,
    name="Upload photos",
    response_model=PhotoListInResponse
)
async def upload_photos(
    files: list[UploadFile],
    gallery_id: Optional[int],
    user: UserInDB = Depends(get_current_user_authorizer()),
    photos_repo: PhotosRepository = Depends(get_repository(PhotosRepository))
) -> PhotoListInResponse:
    # TODO find uuid generation solution for async multiple workers
    list_of_photos = await photos_repo.add_photos_by_user_id(user.id, gallery_id)
    await add_photos(files)
    # TODO error handling for adding files
    return list_of_photos
